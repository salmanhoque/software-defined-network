from mininet.topo import Topo
from mininet.net import Mininet
from mininet.util import irange, dumpNodeConnections
from mininet.link import TCLink
from mininet.log import setLogLevel


class LinearTopo(Topo):
	def __init__(self, linkopts, k=2, **opts):
		super(LinearTopo,self).__init__(**opts)
		
		self.k = k
		
		lastSwitch = None
		for i in irange(1,k):
			host = self.addHost('h%s' %i)
			switch = self.addSwitch('s%s' %i)
			self.addLink(host,switch, **linkopts)
			if lastSwitch:
				self.addLink(switch,lastSwitch)
			lastSwitch = switch

def myTopo():
	linkopts = {'bw':10,'delay':'5ms'}
	topo = LinearTopo(linkopts,k=4)
	net = Mininet(topo=topo,link=TCLink)
	
	net.start()
	print "Dumping host connections"
	dumpNodeConnections(net.hosts)
	print "Testing network connectivity"
	net.pingAll()
	print "Testing bandwidth between h1 and h4"
	net.stop()

if __name__ == '__main__':
	setLogLevel('info')
	myTopo()
