from mininet.net import Mininet
from mininet.topo import LinearTopo

def myTopo():
	linear = LinearTopo(k=4)
	net = Mininet(topo=linear)
	
	net.start()
	net.pingAll()
	net.stop()


if __name__ == '__main__':
	myTopo() 
