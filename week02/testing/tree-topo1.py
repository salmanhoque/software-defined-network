from mininet.topo import Topo
from mininet.net import Mininet
from mininet.topolib import TreeNet
from mininet.util import irange, dumpNodeConnections
from mininet.link import TCLink 
from mininet.log import setLogLevel


class TreeTopo(Topo):
	def __init__(self, linkopts1, linkopts2, linkopts3, depth=3, fanout=2, **opts):
		#super(TreeTopo,self).__init__(**opts)
		Topo.__init__(self,**opts)
		
		aggregateSwitch = []
		edgeSwitch      = [] 
		edgeSwitchCount = 1 
		hostCount    	= 1	
	
		for d in range(depth):
			if d == 0:
				print "Depth is 0"
				coreSwitch = self.addSwitch('c1')
				for i in irange(1,fanout):
					switch = self.addSwitch('a%s' %i)
					aggregateSwitch.append(('a%s' %i))
					self.addLink(coreSwitch,switch,**linkopts1)
			elif d == 1:
				print "Depth is 1"
				for aggSwitch in aggregateSwitch:
					for i in irange(1,fanout):
						switch = self.addSwitch('e%s' %edgeSwitchCount)
						self.addLink(aggSwitch,switch,**linkopts2)
						edgeSwitch.append(('e%s' %edgeSwitchCount))
						edgeSwitchCount = edgeSwitchCount + 1
			elif d == 2:
				print "Depth is 2"	
				for switch in edgeSwitch:
					for i in irange(1,fanout):
						host = self.addHost('h%s' %hostCount)
						self.addLink(switch,host,**linkopts3)
						hostCount = hostCount + 1

def myTopo():
	linkopts1=dict(bw=10,delay='5ms')
	linkopts2=dict(bw=20,delay='6ms')
	linkopts3=dict(bw=30,delay='8ms')
	
	topo = TreeTopo(linkopts1,linkopts2,linkopts3,fanout = 2)
	net = Mininet(topo=topo,link=TCLink)
	
	net.start()
	print "Dumping host connections"
	dumpNodeConnections(net.hosts)
	print "Testing network connectivity"
	net.pingAll()
	net.stop()

if __name__ == '__main__':
	setLogLevel('info')
	myTopo()
